<?php
/**
 * The template for displaying home (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Home_Page
 *
 * @package KarineGallery
 */

get_header();
?>

<div id="primary" class="content-area">
		<main id="main" class="site-main">

          <?php if (have_posts()) : ?>
          <?php if (is_home() && !is_front_page()) :
          ?>
            <header>
             <h1 class="page-title screen-reader-text"><?php single_post_title(); ?></h1>
            </header>
           <?php
          endif; ?>

						<!-- Get the last 2 posts i -->
				<?php $args = array(
				  'post_type'   => 'post',
				  'post_status' => 'publish',
				  'posts_per_page' => 2,
				  'orderby' => 'post_date',
				  'order' => 'DESC',
				); ?>
				<?php $my_query = new WP_Query( $args ); ?>
				<?php while ( $my_query->have_posts() ) : $my_query->the_post(); ?>
				<!-- Do ... -->
				<?php get_template_part('template-parts/content', get_post_format()); ?>
				<?php endwhile; ?>
				<?php wp_reset_postdata(); ?>
				<?php else : ?>
				<?php get_template_part('template-parts/content', 'none'); ?>
				<?php endif; ?>

	
		</main><!-- #main -->
</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
