<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package KarineGallery
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

			<section class="error-404 not-found">
				<header class="page-header">
					<img src="<?php _e(get_template_directory_uri()); ?>/space.jpg">
					<img src="https://media.giphy.com/media/A9EcBzd6t8DZe/giphy.gif">
					<h1 class="page-title"><?php esc_html_e( 'Oops! That page can&rsquo;t be found.', 'karine-gallery' ); ?></h1>
				</header><!-- .page-header -->

				<div class="page-content">
					<p><?php esc_html_e( 'It looks like nothing was found at this location. Maybe try one of the links below or a search?', 'karine-gallery' ); ?></p>

					<?php
					get_search_form();

					the_widget( 'WP_Widget_Recent_Posts' );
					?>

					<div class="widget widget_categories">
						<h2 class="widget-title"><?php esc_html_e( 'Most Used Categories', 'karine-gallery' ); ?></h2>
						
						<!-- Get the last 5 posts i -->
				<?php $args = array(
				  'post_type'   => 'post',
				  'post_status' => 'publish',
				  'posts_per_page' => 5,
				  'orderby' => 'post_date',
				  'order' => 'DESC',
				); ?>
				<?php $my_query = new WP_Query( $args ); ?>
				<?php while ( $my_query->have_posts() ) : $my_query->the_post(); ?>
				<!-- Do ... -->
				<h2><?php the_title() ?></h2>
				<?php endwhile; ?>
				<?php wp_reset_postdata(); ?>
					
					</div><!-- .widget -->

					<?php
					/* translators: %1$s: smiley */
					$karine_gallery_archive_content = '<p>' . sprintf( esc_html__( 'Try looking in the monthly archives. %1$s', 'karine-gallery' ), convert_smilies( ':)' ) ) . '</p>';
					the_widget( 'WP_Widget_Archives', 'dropdown=1', "after_title=</h2>$karine_gallery_archive_content" );

					the_widget( 'WP_Widget_Tag_Cloud' );
					?>

				</div><!-- .page-content -->
			</section><!-- .error-404 -->

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
